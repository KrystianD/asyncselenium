import asyncio
import cgi
import json
from threading import Thread
from typing import List, Dict, Any, Tuple, Callable
import json as json_module
from urllib.parse import urlsplit, urlunsplit, urlencode

import os
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

XHR_SCRIPT = """
var url = arguments[0];
var headers = arguments[1];
var data = arguments[2];
var method = arguments[3];

var http = new XMLHttpRequest();

http.responseType = 'arraybuffer';
http.open(method, url, true);

for (var key in headers) {
    if (headers.hasOwnProperty(key)) {
        http.setRequestHeader(key, headers[key]);
    }
}

http.onreadystatechange = function() {
    if (http.readyState === 4) { // DONE
        var data = {
            "status_code": http.status,
            "headers": http.getAllResponseHeaders(),
            "response": [].slice.call(new Uint8Array(http.response))
        };
        callback(data);
    }
};

http.send(data);
"""


def selenium_task(fn):
    async def wrapper(self: 'SeleniumBrowser', *args, **kwargs):
        main_loop = asyncio.get_event_loop()
        f = asyncio.run_coroutine_threadsafe(fn(self, *args, **kwargs), loop=self.thread_loop)
        return await asyncio.wrap_future(f, loop=main_loop)

    return wrapper


class SeleniumElement:
    def __init__(self, browser: 'SeleniumBrowser', el: WebElement) -> None:
        self.browser = browser
        self.thread_loop = browser.thread_loop
        self.el = el

    @selenium_task
    async def find_elements(self, selector: str, by: By, only_visible=False) -> List['SeleniumElement']:
        els = self.el.find_elements(by, selector)  # type: List[WebElement]

        if only_visible:
            els = [x for x in els if x.is_displayed()]

        return [SeleniumElement(self.browser, x) for x in els]

    @selenium_task
    async def get_attribute(self, selector: str) -> str:
        elm = self.el.get_attribute(selector)  # type: str
        return elm

    @selenium_task
    async def input_text(self, text: str):
        self.el.send_keys(text)

    @selenium_task
    async def send_enter(self):
        self.el.send_keys(Keys.ENTER)

    @selenium_task
    async def click(self, with_js=False):
        if not with_js:
            self.el.click()
        else:
            await self.browser.execute_script("arguments[0].click()", self.el)


class SeleniumResponse:
    def __init__(self, status_code: int, response_bytes: bytes, headers: Dict[str, str]) -> None:
        self.status_code = status_code
        self.response_bytes = response_bytes
        self.headers = {key.lower(): value for key, value in headers.items()}

    def header(self, name: str):
        return self.headers.get(name.lower())

    def bytes(self):
        return self.response_bytes

    def text(self, encoding: str = None):
        if encoding is None:
            content_type = self.header("Content-Type")
            if content_type is not None:
                mime, params = cgi.parse_header(content_type)
                if "charset" in params:
                    encoding = params["charset"]

                if encoding is None:
                    if mime == "application/json":
                        encoding = "UTF-8"

        if encoding is None:
            encoding = "ASCII"

        return self.response_bytes.decode(encoding)

    def json(self):
        return json.loads(self.text())


class SeleniumBrowser:
    def __init__(self) -> None:
        self.browser: webdriver.Chrome = None
        self.thread_loop = None
        self.thread = None

        self.thread_loop = asyncio.new_event_loop()

        def f():
            asyncio.set_event_loop(self.thread_loop)
            self.thread_loop.run_forever()

        self.thread = Thread(target=f)
        self.thread.daemon = True
        self.thread.start()

    @selenium_task
    async def run_browser(self, no_sandbox=False, driver='chrome'):
        if driver == 'chrome':
            chrome_options = webdriver.ChromeOptions()
            if no_sandbox:
                chrome_options.add_argument('--no-sandbox')
            self.browser = webdriver.Chrome(chrome_options=chrome_options)
        elif driver == 'firefox':
            self.browser = webdriver.Firefox(log_path=os.devnull)

    @selenium_task
    async def quit(self):
        if self.browser is not None:
            self.browser.quit()

    def stop_loop(self):
        self.thread_loop.call_soon_threadsafe(lambda: self.thread_loop.stop())
        self.thread.join()

    @selenium_task
    async def go(self, url: str):
        self.browser.get(url)

    # backward compatibility
    @selenium_task
    async def find_elements_by_css_selector(self, selector: str, only_visible=False) -> List[SeleniumElement]:
        els = self.browser.find_elements_by_css_selector(selector)  # type: List[WebElement]

        if only_visible:
            els = [x for x in els if x.is_displayed()]

        return [SeleniumElement(self, x) for x in els]

    @selenium_task
    async def find_elements(self, selector: str, by: By, only_visible=False) -> List[SeleniumElement]:
        els = self.browser.find_elements(by, selector)  # type: List[WebElement]

        if only_visible:
            els = [x for x in els if x.is_displayed()]

        return [SeleniumElement(self, x) for x in els]

    @selenium_task
    async def run_async_script(self, script: str, args=None, timeout=60):
        if args is None:
            args = []

        self.browser.set_script_timeout(timeout)
        script = "var callback = arguments[arguments.length - 1];\n" + script
        return self.browser.execute_async_script(script, *args)

    @selenium_task
    async def wait_for_element(self, string_to_wait: str, by: By, seconds=60, wait_after_find=0) -> SeleniumElement:
        """ waits for element to be available on website
        :param string_to_wait: selector to wait for
        :param by: what is the selector class
        :param seconds: maximum number of seconds to wait for element
        :param wait_after_find: implicit time to wait after finding an element
        :return: found element
        """
        element = WebDriverWait(self.browser, seconds) \
            .until(
                expected_conditions.presence_of_element_located(
                        (by, string_to_wait)
                )
        )
        element = SeleniumElement(self, element)
        await asyncio.sleep(wait_after_find)
        return element

    @selenium_task
    async def wait_for_element_condition(self,
                                         string_to_wait: str,
                                         by: By,
                                         condition: Callable[[Tuple[By, str]], WebElement],
                                         seconds=60,
                                         wait_after_find=0) -> SeleniumElement:
        """ waits for element to be available on website with expected state this element is in
        :param string_to_wait: selector to wait for
        :param by: what is the selector class
        :param condition: what to wait for. class from selenium.webdriver.support.expected_conditions
        :param seconds: maximum number of seconds to wait for element
        :param wait_after_find: implicit time to wait after finding an element
        :return: found element
        """

        await self.wait_for_element(string_to_wait, by, seconds, 0)
        element = WebDriverWait(self.browser, seconds) \
            .until(
                condition((by, string_to_wait))
        )
        element = SeleniumElement(self, element)
        await asyncio.sleep(wait_after_find)
        return element

    @selenium_task
    async def execute_script(self, script: str, element: WebElement):
        result = self.browser.execute_script(script, element)
        return result

    @selenium_task
    async def switch_frame(self, frame: SeleniumElement):
        self.browser.switch_to.frame(frame.el)

    @selenium_task
    async def get_location(self):
        return self.browser.execute_script("return self.location")

    async def perform_xhr(self,
                          url: str,
                          method: str,
                          headers: Dict[str, str] = None,
                          params: Dict[str, str] = None,
                          data: Dict[str, str] = None,
                          json: Any = None,
                          timeout=60) -> SeleniumResponse:
        if data is not None and json is not None:
            raise AttributeError("data and json cannot be both defined")

        post_data = None

        if headers is None:
            headers = {}

        if params is not None:
            parts = urlsplit(url)
            new_parts = (parts.scheme, parts.netloc, parts.path, urlencode(params), parts.fragment)
            url = urlunsplit(new_parts)

        if data is not None:
            headers["Content-Type"] = "application/x-www-form-urlencoded"
            post_data = urlencode(data)

        if json is not None:
            headers["Content-Type"] = "application/json; charset=UTF-8"
            post_data = json_module.dumps(json)

        resp = await self.run_async_script(XHR_SCRIPT, args=[url, headers, post_data, method], timeout=timeout)

        resp = SeleniumResponse(
                status_code=resp["status_code"],
                headers={y[0]: y[1] for y in (x.split(": ") for x in resp["headers"].strip().split("\r\n"))},
                response_bytes=bytes(resp["response"])
        )

        if resp.status_code // 100 == 2:
            return resp
        else:
            raise Exception(resp)

    async def perform_get(self,
                          url: str,
                          headers: Dict[str, str] = None,
                          params: Dict[str, str] = None,
                          timeout=60) -> SeleniumResponse:
        return await self.perform_xhr(
                url=url,
                method="GET",
                headers=headers,
                params=params,
                timeout=timeout,
        )

    async def perform_post(self,
                           url: str,
                           headers: Dict[str, str] = None,
                           params: Dict[str, str] = None,
                           data: Dict[str, str] = None,
                           json: Any = None,
                           timeout=60) -> SeleniumResponse:
        return await self.perform_xhr(
                url=url,
                method="POST",
                headers=headers,
                params=params,
                data=data,
                json=json,
                timeout=timeout,
        )
