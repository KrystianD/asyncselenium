import asyncio
import hashlib
import unittest

from selenium_browser import SeleniumBrowser


def md5(x):
    return hashlib.md5(x).hexdigest()


def async_wrapper(fn):
    def wrapper(*args, **kwargs):
        async def go():
            await fn(*args, **kwargs)

        asyncio.get_event_loop().run_until_complete(go())

    return wrapper


class BrowserTest(unittest.TestCase):
    @async_wrapper
    async def test_utf8(self):
        browser = SeleniumBrowser()
        await browser.run_browser()

        resp = await browser.perform_get("https://httpbin.org/encoding/utf8")
        txt = resp.text()

        self.assertEqual(type(txt), str)
        self.assertEqual(md5(txt.encode("utf-8")), "b7c8290cecbce9e99f1fe44dbed56019")

        await browser.quit()
        browser.stop_loop()

    @async_wrapper
    async def test_get(self):
        browser = SeleniumBrowser()
        await browser.run_browser()

        resp = await browser.perform_get("https://httpbin.org/get", params={"a": "3", "b": "4"})
        data = resp.json()

        self.assertEqual(data["args"]["a"], "3")
        self.assertEqual(data["args"]["b"], "4")

        await browser.quit()
        browser.stop_loop()

    @async_wrapper
    async def test_post(self):
        browser = SeleniumBrowser()
        await browser.run_browser()

        resp = await browser.perform_post("https://httpbin.org/post",
                                          params={"a": "3", "b": "4"},
                                          headers={"X-Custom":"hdr"},
                                          data={"a1": "ᚠᛇᚻ᛫ᛒᛦ"})
        data = resp.json()

        self.assertEqual(data["args"]["a"], "3")
        self.assertEqual(data["args"]["b"], "4")
        self.assertEqual(data["headers"]["X-Custom"], "hdr")
        self.assertEqual(data["form"]["a1"], "ᚠᛇᚻ᛫ᛒᛦ")

        await browser.quit()
        browser.stop_loop()


if __name__ == "__main__":
    unittest.main()
